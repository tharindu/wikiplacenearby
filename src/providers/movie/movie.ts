import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the MovieProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MovieProvider {

  //Localhost Dev
  base_url : string = "/entertainmentApi";

  //Prod Dev
  // base_url : string = "https://api.themoviedb.org/3";

  apiKey : string = "d3eabd38de462d533dd9730591a85505"
  constructor(public http: HttpClient) {
  }

  public getMovieGenres() : Observable<any>{
    let params = new HttpParams().set('api_key', this.apiKey).set('language', 'en-US');
    let apiUrl = this.base_url+"/genre/movie/list"
    return this.http.get(apiUrl, {params : params}).map(res => {
      return res;
    });
  }

  public getMoviesByGenre(id, page) : Observable<any>{
    let params = new HttpParams().set('api_key', this.apiKey).set('language', 'en-US').set('with_genres', id).set('page', page);
    let apiUrl = this.base_url+"/discover/movie"
    return this.http.get(apiUrl, {params : params}).map(res => {
      return res;
    });
  }

  public getTopRated(page, type) : Observable<any>{
    let params = new HttpParams().set('api_key', this.apiKey).set('language', 'en-US').set('page', page);
    let apiUrl = this.base_url+"/"+type+"/top_rated"
    return this.http.get(apiUrl, {params : params}).map(res => {
      return res;
    });
  }

  public getPopular(page, type) : Observable<any>{
    let params = new HttpParams().set('api_key', this.apiKey).set('language', 'en-US').set('page', page);
    let apiUrl = this.base_url+"/"+type+"/popular"
    return this.http.get(apiUrl, {params : params}).map(res => {
      return res;
    });
  }

  public getTvOnAir(page, type) : Observable<any>{
    let params = new HttpParams().set('api_key', this.apiKey).set('language', 'en-US').set('page', page);
    let apiUrl = this.base_url+"/"+type+"/on_the_air"
    return this.http.get(apiUrl, {params : params}).map(res => {
      return res;
    });
  }

  public getUpcomingMovies(page) : Observable<any>{
    let params = new HttpParams().set('api_key', this.apiKey).set('language', 'en-US').set('page', page);
    let apiUrl = this.base_url+"/movie/upcoming"
    return this.http.get(apiUrl, {params : params}).map(res => {
      return res;
    });
  }

  public getDetails(id, type) : Observable<any>{
    let params = new HttpParams().set('api_key', this.apiKey).set('language', 'en-US').set('append_to_response', 'videos,images');
    let apiUrl = this.base_url+"/"+type+"/"+id
    return this.http.get(apiUrl, {params : params}).map(res => {
      return res;
    });
  }

  public getMovieReviews(id, page, type) : Observable<any>{
    let params = new HttpParams().set('api_key', this.apiKey).set('language', 'en-US').set('page', page);
    let apiUrl = this.base_url+"/"+type+"/"+id+'/reviews'
    return this.http.get(apiUrl, {params : params}).map(res => {
      return res;
    });
  }
  

}
