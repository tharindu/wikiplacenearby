import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { map } from 'rxjs/Rx';
import 'rxjs/add/operator/map'
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the WikiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WikiProvider {

  // Localhost Dev
  base_url : string = "/wikiApi";

  //Prod Dev
  // base_url : string = "https://en.wikipedia.org/w";
  
  constructor(public http: HttpClient) {
  }

  public getSearchQuery(query) : Observable<any>{
    let apiUrl = this.base_url+'/api.php?action=query&format=json&generator=prefixsearch&prop=pageprops%7Cpageimages%7Cdescription&redirects=&ppprop=displaytitle&piprop=thumbnail&pithumbsize=160&pilimit=6&gpssearch='+encodeURIComponent(query)
    return this.http.get(apiUrl).map(res => {
      return res;
    });
  }

  public getNearbyLocations(lat, lng) : Observable<any>{
    let apiUrl = this.base_url+'/api.php?action=query&list=geosearch&gscoord='+lat+'%7C'+lng+'&gsradius=10000&gslimit=100&format=json'
    return this.http.get(apiUrl).map(res => {
      return res;
    });
  }

  public getAdvancedNearbyLocations(lat, lng) : Observable<any>{
    let apiUrl = this.base_url+'/api.php?action=query&format=json&prop=coordinates%7Cpageprops%7Cpageprops%7Cpageimages%7Cdescription&colimit=max&generator=geosearch&ggsradius=10000&ggsnamespace=0&ggslimit=200&formatversion=2&ggscoord='+lat+'%7C'+lng+'&ppprop=displaytitle&piprop=thumbnail&pithumbsize=150&pilimit=200&codistancefrompoint='+lat+'%7C'+lng
    return this.http.get(apiUrl).map(res => {
      return res;
    });
  }

  public getPageInfoFromTitle(title) : Observable<any>{
    let apiUrl = this.base_url+'/api.php?action=parse&page='+title+'&format=json'
    return this.http.get(apiUrl).map(res => {
      return res;
    });
  }

}
