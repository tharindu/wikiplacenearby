import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FavoritesPage } from '../pages/favorites/favorites';
import { WikiProvider } from '../providers/wiki/wiki';
import { HttpClientModule } from '@angular/common/http'; 
import { DistanceToKmPipe } from '../pipes/distance-to-km/distance-to-km';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { DetailPage } from '../pages/detail/detail';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { IonicStorageModule } from '@ionic/storage';
import { DatabaseProvider } from '../providers/database/database';
import { SQLite } from '@ionic-native/sqlite';
import { MovieProvider } from '../providers/movie/movie';
import { EntertainmentPage } from '../pages/entertainment/entertainment/entertainment';
import { BooksPage } from '../pages/books/books';
import { MovieCategoriesPage } from '../pages/entertainment/movie-categories/movie-categories';
import { TvCategoriesPage } from '../pages/entertainment/tv-categories/tv-categories';
import { MoviesGenreComponent } from '../components/tabs/movies-genre/movies-genre';
import { MoviesTopRatedComponent } from '../components/tabs/movies-top-rated/movies-top-rated';
import { MoviesUpcomingComponent } from '../components/tabs/movies-upcoming/movies-upcoming';
import { ListMoviesPage } from '../pages/list-movies/list-movies';
import { ListMoviesComponent } from '../components/list-movies/list-movies';
import { MovieDetailsPage } from '../pages/movie-details/movie-details';
import { MovieDetailsComponent } from '../components/movie-details/movie-details';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { TvLatestComponent } from '../components/tabs/tv-latest/tv-latest';
import { TvPopularComponent } from '../components/tabs/tv-popular/tv-popular';
import { TvTopRatedComponent } from '../components/tabs/tv-top-rated/tv-top-rated';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    FavoritesPage,
    DistanceToKmPipe,
    DetailPage,
    EntertainmentPage,
    BooksPage,
    MovieCategoriesPage,
    TvCategoriesPage,
    MoviesGenreComponent,
    MoviesTopRatedComponent,
    MoviesUpcomingComponent,
    ListMoviesPage,
    ListMoviesComponent,
    MovieDetailsPage,
    MovieDetailsComponent,
    TvLatestComponent,
    TvPopularComponent,
    TvTopRatedComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: 'favoritesDB',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    FavoritesPage,
    DetailPage,
    EntertainmentPage,
    BooksPage,
    MovieCategoriesPage,
    TvCategoriesPage,
    MoviesGenreComponent,
    MoviesTopRatedComponent,
    MoviesUpcomingComponent,
    ListMoviesPage,
    ListMoviesComponent,
    MovieDetailsPage,
    MovieDetailsComponent,
    TvLatestComponent,
    TvPopularComponent,
    TvTopRatedComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    WikiProvider,
    Geolocation,
    Diagnostic,
    InAppBrowser,
    DatabaseProvider,
    Storage,
    SQLite,
    MovieProvider,
    YoutubeVideoPlayer
  ]
})
export class AppModule {}
