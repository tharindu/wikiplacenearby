import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WikiProvider } from '../../providers/wiki/wiki';
import { DetailPage } from '../detail/detail';
import * as _ from 'lodash';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  searchItems: any[];
  errorMessage: any;
  searchQuery : string;

  constructor(public navCtrl: NavController,
    private wikiService : WikiProvider) {

  }

  getItems(val){
    this.searchItems = [];
    if(val){
      this.wikiService.getSearchQuery(val)
      .subscribe(
        places => {
          for (var key in places.query.pages) {
            if (places.query.pages.hasOwnProperty(key)) {
              this.searchItems.push(places.query.pages[key]);
            }
          }
          this.searchItems = _.sortBy(this.searchItems, [(o) => { 
            return o.index; 
          }]);
        },
        error =>  this.errorMessage = <any>error);
    }
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(DetailPage, {
      item: item
    });
  }

}
