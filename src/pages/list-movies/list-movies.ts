import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MovieProvider } from '../../providers/movie/movie';
import * as _ from 'lodash';

/**
 * Generated class for the ListMoviesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-list-movies',
  templateUrl: 'list-movies.html',
})
export class ListMoviesPage {

  type: string;
  infiniteScroll: any;
  page: string = '1';
  listItems: any[] = [];
  mode: any;
  title: any;
  item: any;
  imageUrl : string = "https://image.tmdb.org/t/p/w500/";
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private entertainmentProvider : MovieProvider) {
    this.item = navParams.get('item');
    this.title = this.item.name;
    this.mode = navParams.get('mode');

  }

  ionViewDidLoad() {
    if(this.mode == 'movieGenre'){
      this.type = 'movie';
      this.getMovieListByGenre(this.item.id, this.page);
    } else{
      this.type = 'tv'
    }
  }

  loadMore(event){
    console.log(event);
    this.infiniteScroll = event;
    this.getMovieListByGenre(this.item.id, this.page);
  }

  getMovieListByGenre(id, page){
    this.entertainmentProvider.getMoviesByGenre(id, page).subscribe((results) => {
      this.listItems = _.concat(this.listItems,results.results);
      this.page = results.page+1;
      if(this.infiniteScroll)
        this.infiniteScroll.complete();
    });
  }

}
