import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MovieProvider } from '../../providers/movie/movie';

/**
 * Generated class for the MovieDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-movie-details',
  templateUrl: 'movie-details.html',
})
export class MovieDetailsPage {

  mode: any;
  mediaSegment: string;
  movieDetails: any;
  selectedItem: any;
  imageUrl : string = "https://image.tmdb.org/t/p/w500/";
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private entertainmentProvider : MovieProvider) {
    this.selectedItem = navParams.get('item');
    this.mode = navParams.get('mode');
  }

  ionViewDidLoad() {
    this.getMovieDetails(this.selectedItem, this.mode);
  }

  getMovieDetails(item, mode){
    this.entertainmentProvider.getDetails(item.id, mode).subscribe((res:any) => {
      this.movieDetails = res;
    })
  }

}
