import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MoviesGenreComponent } from '../../../components/tabs/movies-genre/movies-genre';
import { MoviesTopRatedComponent } from '../../../components/tabs/movies-top-rated/movies-top-rated';
import { MoviesUpcomingComponent } from '../../../components/tabs/movies-upcoming/movies-upcoming';

/**
 * Generated class for the MovieCategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-movie-categories',
  templateUrl: 'movie-categories.html',
})

export class MovieCategoriesPage {

  movieGenreTab : any;
  movieTopRatedTab : any;
  movieUpcomingTab : any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.movieGenreTab = MoviesGenreComponent
    this.movieTopRatedTab = MoviesTopRatedComponent
    this.movieUpcomingTab = MoviesUpcomingComponent
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MovieCategoriesPage');
  }

}
