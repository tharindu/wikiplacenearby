import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MovieCategoriesPage } from '../movie-categories/movie-categories';
import { TvCategoriesPage } from '../tv-categories/tv-categories';

/**
 * Generated class for the EntertainmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-entertainment',
  templateUrl: 'entertainment.html',
})
export class EntertainmentPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EntertainmentPage');
  }

  goToType(view){
    if(view == 'movie')
      this.navCtrl.push(MovieCategoriesPage);
    else 
      this.navCtrl.push(TvCategoriesPage);
  }
}
