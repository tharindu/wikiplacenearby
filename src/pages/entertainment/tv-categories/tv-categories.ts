import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TvLatestComponent } from '../../../components/tabs/tv-latest/tv-latest';
import { TvPopularComponent } from '../../../components/tabs/tv-popular/tv-popular';
import { TvTopRatedComponent } from '../../../components/tabs/tv-top-rated/tv-top-rated';
import { MoviesTopRatedComponent } from '../../../components/tabs/movies-top-rated/movies-top-rated';

/**
 * Generated class for the TvCategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-tv-categories',
  templateUrl: 'tv-categories.html',
})
export class TvCategoriesPage {

  tvOnAirTab : any;
  tvPopularTab : any;
  tvTopRatedTab : any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tvOnAirTab = TvLatestComponent
    this.tvPopularTab = TvPopularComponent
    this.tvTopRatedTab = TvTopRatedComponent
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TvCategoriesPage');
  }

}
