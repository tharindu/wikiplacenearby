import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { WikiProvider } from '../../providers/wiki/wiki';
import * as _ from 'lodash';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';
import { DetailPage } from '../detail/detail';
import { Diagnostic } from '@ionic-native/diagnostic';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  favoriteArray: any[] = [];
  _ : any;
  loader: any;
  errorMessage: any;
  selectedItem: any;
  icons: string[];
  items: any = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private wikiService : WikiProvider,
    private geolocation: Geolocation,
    private platform: Platform,
    private diagnostic: Diagnostic,
    private alertCtrl : AlertController,
    public loadingCtrl: LoadingController,
    private storage : Storage) {
      this._ = _;
  }
  
  ionViewDidLoad(){
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    this.loader.present();
    if(this.platform.is('cordova')){
      this.diagnostic.isGpsLocationEnabled().then(state => {
        if (!state) {
          let confirm = this.alertCtrl.create({
            title: '<b>Location</b>',
            message: 'Location information is unavaliable on this device. Go to Settings to enable Location.',
            buttons: [
              {
                text: 'cancel',
                role: 'Cancel',
                handler: () => {
                  this.navCtrl.push(HomePage); // this is optional(you can use only one button if maps is necessary) according to your needs if you want to navigate user to some other place if he does not give location access.
                }
              },
              {
                text: 'Go to settings',
                handler: () => {
                  this.diagnostic.switchToLocationSettings()
                }
              }
            ]
          });
          confirm.present();
        } else {
          this.platform.ready().then(()=>{
            //set options.. 
            var options = {
              enableHighAccuracy: true,
              timeout: 5000,
              maximumAge: 0
            };
            //use the geolocation 
            this.geolocation.getCurrentPosition(options).then(data=>{
              // debugger
              this.getNearbyList(data.coords.latitude, data.coords.longitude);
            }).catch((err)=>{
              console.log("Error", err);
            });
          });
        }
      })
    } else {
      this.platform.ready().then(()=>{
        //set options.. 
        var options = {
          enableHighAccuracy: true,
          timeout: 5000,
          maximumAge: 0
        };
        //use the geolocation 
        this.geolocation.getCurrentPosition(options).then(data=>{
          // debugger
          this.getNearbyList(data.coords.latitude, data.coords.longitude);
        }).catch((err)=>{
          console.log("Error", err);
        });
      });
    }
  }

  getNearbyList(lat, lng) {
    this.wikiService.getAdvancedNearbyLocations(lat, lng)
      .subscribe(
        places => {
          this.items = places.query.pages;
          this.items = _.sortBy(this.items, [(o) => { 
            return o.coordinates[0].dist; 
          }]);
          this.loader.dismiss()
        },
        error =>  {
          this.errorMessage = <any>error
          this.loader.dismiss()
        });
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(DetailPage, {
      item: item
    });
  }

  setFavorite(item){
    console.log(item);
    this.favoriteArray = [];
    this.storage.get('favorites').then((val) => {
      if(val)
        this.favoriteArray = val;
      var match = _.filter(this.favoriteArray, (o)  => {
        return o.pageid == item.pageid; 
      });
  
      if(match.length){
        _.remove(this.favoriteArray, (o) => {
          return o.pageId == match[0].pageId
        });
      } else {
        this.favoriteArray.push(item);
      }
      this.storage.set('favorites', this.favoriteArray);
    });
  }
}
