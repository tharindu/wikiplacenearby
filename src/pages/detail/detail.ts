import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { WikiProvider } from '../../providers/wiki/wiki';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import * as _ from 'lodash';
import { LoadingController } from 'ionic-angular';
/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
  host: {
    '(document:click)': 'onClick($event)',
  }
})
export class DetailPage {
  loader: any;
  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
  }
  title: string;
  errorMessage: any;
  details: any;
  selectedItem: any;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private wikiService : WikiProvider,
    private iab: InAppBrowser,
    public loadingCtrl: LoadingController) {
    this.selectedItem = navParams.get('item');
    this.wikiService.getPageInfoFromTitle(encodeURIComponent(this.selectedItem.title.replace(/ /g,"_")))
      .subscribe(
        detail => {
          this.title = detail.parse.title;
          this.details = detail.parse.text['*'];
          this.loader.dismiss();
        },
        error =>  {
          this.errorMessage = <any>error
          this.loader.dismiss()
        });
  }

  ionViewDidLoad() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    this.loader.present();
  }

  onClick(event) {
    event.preventDefault()
    var item = {}
    if(event.target.tagName.toLowerCase() === 'a'){
      if(event.target.innerHTML !== 'edit' && event.target.pathname !== '/'){
        item = {
          title : event.target.title
        }
        if(!this.navCtrl.isTransitioning()){
          this.navCtrl.push(DetailPage, {
            item: item
          })
        }
      }
    } else if(event.target.tagName.toLowerCase() === 'i'){
      if(event.target.innerHTML !== 'edit'){
        var title = event.target.parentNode.title
        item = {
          title : title
        }
        if(!this.navCtrl.isTransitioning()){
          this.navCtrl.push(DetailPage, {
            item: item
          })
        }
      }
    } else if(event.target.tagName.toLowerCase() === 'img'){
      this.openWithInAppBrowser(event.target.currentSrc);
    } else {
      if(event.target.className == 'toctext'){
        var elements = this.contains('span', event.target.innerHTML);
        if(elements.length){
          var matchElement = _.filter(elements, (o) => {
            return o.className == 'mw-headline';
          })
          if(matchElement){
            matchElement[0].scrollIntoView({ behavior: 'smooth', block: 'start' })
          }
        }
      }
    }
  }

  contains(selector, text) {
    var elements = document.querySelectorAll(selector);
    return Array.prototype.filter.call(elements, function(element){
      return RegExp(text).test(element.textContent);
    });
  }

  openWithInAppBrowser(url : string){
    let target = "_blank";
    this.iab.create(url,target,this.options);
  }

}
