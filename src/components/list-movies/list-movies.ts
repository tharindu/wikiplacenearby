import { Component, Input, OnChanges } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MovieDetailsPage } from '../../pages/movie-details/movie-details';
import { MovieProvider } from '../../providers/movie/movie';
import * as _ from 'lodash';

/**
 * Generated class for the ListMoviesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'list-movies',
  templateUrl: 'list-movies.html'
})
export class ListMoviesComponent implements OnChanges{

  infiniteScroll: any;
  imageUrl : string = "https://image.tmdb.org/t/p/w500/";
  @Input('items') items: any;
  @Input('mode') mode: any;

  constructor(private navCtrl : NavController,
    private entertainmentProvider : MovieProvider) {
  }
  
  ngOnChanges(){
    console.log('Hello ListMoviesComponent Component', this.mode);
  }

  viewDetails(item){
    if(this.navCtrl.parent){
      this.navCtrl.parent.parent.push(MovieDetailsPage, 
      {
        'item' : item,
        'mode' : this.mode
      });
    } else {
      this.navCtrl.push(MovieDetailsPage, 
      {
        'item' : item,
        'mode' : this.mode
      });
    }
  }

}
