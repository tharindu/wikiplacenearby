import { Component } from '@angular/core';
import { MovieProvider } from '../../../providers/movie/movie';
import * as _ from 'lodash';

/**
 * Generated class for the TvLatestComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'tv-latest',
  templateUrl: 'tv-latest.html'
})
export class TvLatestComponent {

  infiniteScroll: any;
  page: string = '1';
  listItems: any[] = [];
  mode : string = 'tv'

  constructor(private entertainmentProvider : MovieProvider) {
    this.getLatestTvList(this.page, this.mode);
  }

  getLatestTvList(page, mode){
    this.entertainmentProvider.getTvOnAir(page, mode).subscribe((results) => {
      this.listItems = _.concat(this.listItems,results.results);
      this.page = results.page+1;
      if(this.infiniteScroll)
        this.infiniteScroll.complete();
    });
  }

  loadMore(event){
    console.log(event);
    this.infiniteScroll = event;
    this.getLatestTvList(this.page, this.mode);
  }

}
