import { Component } from '@angular/core';
import { MovieProvider } from '../../../providers/movie/movie';
import { NavController } from 'ionic-angular';
import { ListMoviesPage } from '../../../pages/list-movies/list-movies';

/**
 * Generated class for the MoviesTabsGenreComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'movies-genre',
  templateUrl: 'movies-genre.html'
})
export class MoviesGenreComponent {

  genres: any;
  text: string;

  constructor(private entertainmentProvider : MovieProvider,
  private navCtrl : NavController) {
    this.entertainmentProvider.getMovieGenres().subscribe((results) => {
      this.genres = results.genres;
    })
  }

  itemSelected(genre){
    this.navCtrl.parent.parent.push(ListMoviesPage, {
      item : genre,
      mode : 'movieGenre'
    })
  }

}
