import { Component } from '@angular/core';
import { MovieProvider } from '../../../providers/movie/movie';
import * as _ from 'lodash';

/**
 * Generated class for the TvTopRatedComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'tv-top-rated',
  templateUrl: 'tv-top-rated.html'
})
export class TvTopRatedComponent {

  infiniteScroll: any;
  page: string = '1';
  listItems: any[] = [];
  mode : string = 'tv'

  constructor(private entertainmentProvider : MovieProvider) {
    this.getTopRatedTvList(this.page, this.mode)
  }

  getTopRatedTvList(page, mode){
    this.entertainmentProvider.getTopRated(page, mode).subscribe((results) => {
      this.listItems = _.concat(this.listItems,results.results);
      this.page = results.page+1;
      if(this.infiniteScroll)
        this.infiniteScroll.complete();
    });
  }

  loadMore(event){
    console.log(event);
    this.infiniteScroll = event;
    this.getTopRatedTvList(this.page, this.mode);
  }

}
