import { Component, Input } from '@angular/core';
import { MovieProvider } from '../../../providers/movie/movie';
import * as _ from 'lodash';

/**
 * Generated class for the MoviesTopRatedComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'movies-top-rated',
  templateUrl: 'movies-top-rated.html'
})

export class MoviesTopRatedComponent {
  
  infiniteScroll: any;
  page: string = '1';
  listItems: any[] = [];
  mode : string = 'movie'

  constructor(private entertainmentProvider : MovieProvider) {
    this.getTopRatedMovieList(this.page, this.mode);
  }  

  getTopRatedMovieList(page, mode){
    this.entertainmentProvider.getTopRated(page, mode).subscribe((results) => {
      this.listItems = _.concat(this.listItems,results.results);
      this.page = results.page+1;
      if(this.infiniteScroll)
        this.infiniteScroll.complete();
    });
  }

  loadMore(event){
    console.log(event);
    this.infiniteScroll = event;
    this.getTopRatedMovieList(this.page, this.mode);
  }
  

}
