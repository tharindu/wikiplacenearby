import { Component } from '@angular/core';
import { MovieProvider } from '../../../providers/movie/movie';
import * as _ from 'lodash';

/**
 * Generated class for the TvPopularComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'tv-popular',
  templateUrl: 'tv-popular.html'
})
export class TvPopularComponent {

  infiniteScroll: any;
  page: string = '1';
  listItems: any[] = [];
  mode : string = 'tv'

  constructor(private entertainmentProvider : MovieProvider) {
    this.getPopularTvList(this.page, this.mode);
  }

  getPopularTvList(page, mode){
    this.entertainmentProvider.getPopular(page, mode).subscribe((results) => {
      this.listItems = _.concat(this.listItems,results.results);
      this.page = results.page+1;
      if(this.infiniteScroll)
        this.infiniteScroll.complete();
    });
  }

  loadMore(event){
    console.log(event);
    this.infiniteScroll = event;
    this.getPopularTvList(this.page, this.mode);
  }

}
