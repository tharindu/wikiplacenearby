import { Component } from '@angular/core';
import { MovieProvider } from '../../../providers/movie/movie';
import * as _ from 'lodash';

/**
 * Generated class for the MoviesUpcomingComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'movies-upcoming',
  templateUrl: 'movies-upcoming.html'
})
export class MoviesUpcomingComponent {

  infiniteScroll: any;
  page: string = '1';
  listItems: any[] = [];
  imageUrl : string = "https://image.tmdb.org/t/p/w500/";
  mode : string = 'movie'

  constructor(private entertainmentProvider: MovieProvider) {
    this.getTopUpcomingMovieList(this.page);
  }

  loadMore(event){
    console.log(event);
    this.infiniteScroll = event;
    this.getTopUpcomingMovieList(this.page);
  }

  getTopUpcomingMovieList(page){
    this.entertainmentProvider.getUpcomingMovies(page).subscribe((results) => {
      this.listItems = _.concat(this.listItems,results.results);
      this.page = results.page+1;
      if(this.infiniteScroll)
        this.infiniteScroll.complete();
    });
  }
}
