import { NgModule } from '@angular/core';
import { MoviesGenreComponent } from './tabs/movies-genre/movies-genre';
import { MoviesTopRatedComponent } from './tabs/movies-top-rated/movies-top-rated';
import { MoviesUpcomingComponent } from './tabs/movies-upcoming/movies-upcoming';
import { ListMoviesComponent } from './list-movies/list-movies';
import { MovieDetailsComponent } from './movie-details/movie-details';
import { TvLatestComponent } from './tabs/tv-latest/tv-latest';
import { TvPopularComponent } from './tabs/tv-popular/tv-popular';
import { TvTopRatedComponent } from './tabs/tv-top-rated/tv-top-rated';
@NgModule({
	declarations: [MoviesGenreComponent,
    MoviesTopRatedComponent,
    MoviesUpcomingComponent,
    ListMoviesComponent,
    MovieDetailsComponent,
    TvLatestComponent,
    TvPopularComponent,
    TvTopRatedComponent],
	imports: [],
	exports: [MoviesGenreComponent,
    MoviesTopRatedComponent,
    MoviesUpcomingComponent,
    ListMoviesComponent,
    MovieDetailsComponent,
    TvLatestComponent,
    TvPopularComponent,
    TvTopRatedComponent,
]
})
export class ComponentsModule {}
