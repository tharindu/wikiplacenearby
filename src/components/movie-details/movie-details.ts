import { Component, Input, OnChanges } from '@angular/core';
import { MovieProvider } from '../../providers/movie/movie';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

/**
 * Generated class for the MovieDetailsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'movie-details',
  templateUrl: 'movie-details.html'
})
export class MovieDetailsComponent implements OnChanges {

  infiniteScroll: any;
  mediaSegment: string;
  imageUrl : string = "https://image.tmdb.org/t/p/w500/";
  @Input('item') item: any;
  @Input('mode') mode: any;
  movieReviews : any;
  page: string = '1';
  constructor(private entertainmentProvider: MovieProvider,
    private youtube: YoutubeVideoPlayer) {
    this.mediaSegment = "info";
  }
  
  loadMoreReviews(event){
    console.log(event);
    this.infiniteScroll = event;
    this.getMovieReviews(this.item, this.page, this.mode);
  }
  
  ngOnChanges(){
    console.log("this.mode ", this.mode);
    if(this.item && this.mode){
      this.getMovieReviews(this.item, this.page, this.mode);
    }
  }

  getMovieReviews(item, page, mode){
    this.entertainmentProvider.getMovieReviews(item.id, page, mode).subscribe((res:any) => {
      this.movieReviews = res;
      this.page = res.page+1;
      if(this.infiniteScroll)
        this.infiniteScroll.complete();
    })
  }

  playVideo(video){
    this.youtube.openVideo(video.key);
  }

}
