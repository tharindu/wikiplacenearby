import { NgModule } from '@angular/core';
import { DistanceToKmPipe } from './distance-to-km/distance-to-km';
@NgModule({
	declarations: [DistanceToKmPipe],
	imports: [],
	exports: [DistanceToKmPipe]
})
export class PipesModule {}
