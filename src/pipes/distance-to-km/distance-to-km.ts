import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'distanceToKm'
})
export class DistanceToKmPipe implements PipeTransform {

  transform(value: number): string {
    var distance: string = (value / 1000).toFixed(1).toString();

    if(value < 1000){
      return distance + 'M';
    } else if(value == 1000){
      return distance + ' KM';
    }else {
      return distance + ' KM';
    }
 }

}
